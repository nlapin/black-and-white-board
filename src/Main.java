public class Main {

    private static final int DESK_SIZE = 8;

    private static final char BLACK_MARKER = 'B';
    private static final char WHITE_MARKER = 'W';


    public static void main(String[] args) {

        char[][] chessDesk = new char[DESK_SIZE][DESK_SIZE];

        for (int i = 0; i < chessDesk.length; i++) {
            chessDesk[i] = fillingLine(chessDesk[i], i);
        }
    }

    private static char[] fillingLine(char[] line, int currentLine) {
        int lineLength = line.length;

        for (int i = 0; i < lineLength; i++) {
            line[i] = (i + currentLine) % 2 == 0 ? BLACK_MARKER : WHITE_MARKER;
            System.out.print(line[i] + " ");
        }
        System.out.println();
        return line;
    }

}